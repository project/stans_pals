CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

Stans Pals is a Subtheme of the business theme designed firstly to make the view
a little less formal. Secondly to change the theme to have certain dynamic 
scaling elements to fit the size of the theme. There is a second theme 
"Stans Pals Mobile" which focuses on extending Stans Pals to with a more mobile
friendly slant but this is not required as the standard theme will still 
dynamically scale much smaller than the average desktop size.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/bigmonmulgrew/2547771

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/2547771
   
REQUIREMENTS
------------

This theme requires the following parent theme:

 * Business Theme (https://www.drupal.org/project/business)

This theme may optionally be supported by:

 * Stans Pals Mobile (https://www.drupal.org/sandbox/bigmonmulgrew/2547829)
 
 INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal theme. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.   
   
CONFIGURATION
-------------

 * To enable the theme go to http://yoursite.co.uk/admin/appearance 
 * Scroll down to locat the theme.
 * Click Enable and Set Default
 
MAINTAINERS
-----------

Current maintainers:
 * David Mulgrew (bigmonmulgrew) - https://drupal.org/u/bigmonmulgrew

This project has been sponsored by:
 * MULGREW ENTERPRISES
   Specialized in IT management, consulting and planning.
   Visit http://foreverythingit.co.uk for more information.
