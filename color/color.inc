<?php
/**
 * @file
 * Sets up the ability to change theme colours in the UI.
 */

$info = array(
  // Available colors and color labels used in theme.
  'fields' => array(
    'bg' => t('Main background'),
    'menu' => t('Menu'),
    'menu_hover' => t('Menu Hover'),
    'link' => t('Link'),
    'link_hover' => t('Link Hover'),
    'button' => t('Button'),
    'button_hover' => t('Button Hover'),
  ),

  // Pre-defined color schemes.
  'schemes' => array(
    'default' => array(
      'title' => t('Default'),
      'colors' => array(
        'bg' => '#96c8c8',
        'menu' => '#96c8c8',
        'menu_hover' => '#96c8c8',
        'link' => '#2E70B1',
        'link_hover' => '#96c8c8',
        'button' => '#96c8c8',
        'button_hover' => '#0067BF',
      ),
    ),
    // New Scheme.
  ),

  // CSS files (excluding @import) to rewrite with new color scheme.
  'css' => array(
    'color/colors.css',
  ),

  // Files to copy.
  'copy' => array(
    'logo.png',
  ),

  // Gradient definitions.
  'gradients' => array(),

  // Color areas to fill (x, y, width, height).
  'fill' => array(),

  // Coordinates of all the theme slices (x, y, width, height)
  // with their filename as used in the stylesheet.
  'slices' => array(),

  // Reference color used for blending. Matches the base.png's colors.
  'blend_target' => '#ffffff',

  // Preview files.
  'preview_css' => 'color/preview.css',
  'preview_js' => 'color/preview.js',
  'preview_html' => 'color/preview.html',

  // Base file for image generation.
  'base_image' => 'color/base.png',
);
